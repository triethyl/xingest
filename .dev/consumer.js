const amqplib = require("amqplib");

var amqp_url =
  process.env.CLOUDAMQP_URL || "amqp://username:password@localhost:5672/";

async function do_consume() {
  var conn = await amqplib.connect(amqp_url, "heartbeat=60");
  var ch = await conn.createChannel();
  var q = "maco-gps-event";
  await conn.createChannel();
  await ch.assertQueue(q, { durable: true });
  await ch.consume(q, function (msg) {
    console.log(msg.content.toString());
    ch.ack(msg);
  });
}

do_consume().catch(console.error);
