FROM node:12-slim

LABEL by="Sandipan, Sayan, Kushankur"
LABEL for="Durbin Technologies"

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

RUN yarn

EXPOSE 4000

CMD ["yarn", "serve"]