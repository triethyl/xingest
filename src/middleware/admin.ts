import { Handler } from "express";
import jwt from "jsonwebtoken";

import customError from "../utils/customError";

import { JWT_SIGNKEY, JWT_EXPIRESIN } from "../config";

const adminLoggedIn: Handler = async (req, res, next) => {
  try {
    const { uid } = req.cookies;

    if (!uid) throw Error;

    const { username } = jwt.verify(uid, JWT_SIGNKEY) as { username: string };

    res.cookie(
      "uid",
      jwt.sign({ username }, JWT_SIGNKEY, { expiresIn: JWT_EXPIRESIN }),
      { httpOnly: true }
    );

    next();
  } catch {
    res.clearCookie("uid");
    customError(
      res,
      '"uid" not present or invalid - not logged in',
      "uid",
      "not_logged_in",
      null
    );
  }
};

export default adminLoggedIn;
