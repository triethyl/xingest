import { Handler } from "express";
import { getConnection } from "typeorm";

import Device from "../entity/Device";

const device_auth: Handler = async (req, res, next) => {
  let id = req.headers["xingest-id"];

  if (typeof id === "object") id = id[0];
  if (!id) return res.sendStatus(400);

  const r = /(?<platform>.*)\/(?<group>.*)\/(?<deviceId>.*)::(?<devicePassword>.*)/g;
  const match = r.exec(id);

  if (
    !match ||
    !match.groups ||
    Object.getOwnPropertyNames(match.groups).length !== 4
  )
    return res.sendStatus(400);

  const groups = match.groups;

  const dev = await getConnection()
    .createQueryBuilder(Device, "device")
    .leftJoinAndSelect("device.group", "group")
    .where("group.platform = :platform", { platform: groups["platform"] })
    .andWhere("group.name = :group", { group: groups["group"] })
    .andWhere("device.deleted = :deviceDelete", { deviceDelete: false })
    .andWhere("device.disabled = :deviceDisable", { deviceDisable: false })
    .andWhere("device.device_id = :deviceId", {
      deviceId: groups["deviceId"],
    })
    .andWhere("device.device_password = :devicePassword", {
      devicePassword: groups["devicePassword"],
    })
    .getOne();

  if (!dev) return res.sendStatus(400);

  // @ts-ignore
  req.source = { ...groups };

  req.source.deviceHash = dev.device_hash;
  req.source.groupRecordId = dev.group.id;
  req.source.deviceRecordId = dev.id;

  next();
};

export default device_auth;
