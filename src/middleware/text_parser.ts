import { Handler } from "express";

const textParser: Handler = function (req, _, next) {
  if (req.is("text/*")) {
    req.text = "";
    req.setEncoding("utf8");
    req.on("data", function (chunk: string) {
      req.text += chunk;
    });
    req.on("end", next);
  } else {
    next();
  }
};

export default textParser;
