import { Handler } from "express";
import { getConnection } from "typeorm";

import Event from "../entity/Event";

const device_auth = (body: string): Handler => async (req, res, next) => {
  let id = req.headers["xingest-id"];

  if (typeof id === "object") id = id[0];
  if (!id) return res.sendStatus(400);

  const r = /(?<platform>.*)\/(?<group>.*)\/(?<event>.*)\/(?<deviceId>.*)::(?<devicePassword>.*)/g;
  const match = r.exec(id);

  if (
    !match ||
    !match.groups ||
    Object.getOwnPropertyNames(match.groups).length !== 5
  )
    return res.sendStatus(400);

  const groups = match.groups;

  const ev = await getConnection()
    .createQueryBuilder(Event, "event")
    .leftJoinAndSelect("event.mq", "mq")
    .leftJoinAndSelect("event.group", "group")
    .leftJoinAndSelect("group.device", "device")
    .where("group.platform = :platform", { platform: groups["platform"] })
    .andWhere("group.name = :group", { group: groups["group"] })
    .andWhere("event.name = :event", { event: groups["event"] })
    .andWhere("event.body = :eventType", { eventType: body })
    .andWhere("event.deleted = :eventDelete", { eventDelete: false })
    .andWhere("device.deleted = :deviceDelete", { deviceDelete: false })
    .andWhere("device.disabled = :deviceDisable", { deviceDisable: false })
    .andWhere("device.device_id = :deviceId", {
      deviceId: groups["deviceId"],
    })
    .andWhere("device.device_password = :devicePassword", {
      devicePassword: groups["devicePassword"],
    })
    .getOne();

  if (!ev) return res.sendStatus(400);

  // @ts-ignore
  req.source = { ...groups };

  req.source.eventRecordId = ev.id;
  req.source.groupRecordId = ev.group.id;
  req.source.deviceHash = ev.group.device[0].device_hash;
  req.source.deviceRecordId = ev.group.device[0].id;
  req.source.mqs = ev.mq.map((mq) => mq.name);

  req.source.eventFileMimetype = ev.file_mimetype;
  req.source.eventAllowMultipleFiles = ev.file_multiple || false;

  next();
};

export default device_auth;
