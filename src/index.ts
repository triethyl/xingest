import init from "./utils/init";
import app from "./utils/server";
import log from "./log";

import { APP_PORT } from "./config";

import "reflect-metadata";

interface Source {
  platform: string;
  group: string;
  event: string;
  deviceId: string;
  devicePassword: string;
  deviceHash: string;
  groupRecordId: string;
  eventRecordId: string;
  deviceRecordId: string;
  mqs: string[];
  eventFileMimetype: string | null | undefined;
  eventAllowMultipleFiles: boolean;
}

declare global {
  namespace Express {
    interface Request {
      text: string;
      source: Source;
    }
  }
}

const main = async () => {
  await init();

  app.listen(APP_PORT, "0.0.0.0", () =>
    log.info(`🚀 Server listening on port ${APP_PORT}`)
  );
};

main().catch(log.error);
