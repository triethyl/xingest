import redis from "redis";

import { REDIS_HOST, REDIS_PASSWORD, REDIS_PORT } from "../config";

const client = redis.createClient({
  host: REDIS_HOST,
  password: REDIS_PASSWORD,
  port: REDIS_PORT,
});

export default client;
