import amqplib from "amqplib";

import { MQ_URI } from "../config";

import log from "../log";

class MQ {
  // @ts-ignore
  private conn: amqplib.Connection | null;
  private ch: amqplib.Channel | null;

  constructor() {
    this.ch = null;
    this.setupConn().catch(log.error);
    this.publish.bind(this);
  }

  async setupChannel() {
    if (!this.conn) return;
    this.ch = await this.conn.createChannel();
    this.ch.on("close", async () => {
      log.info("MQ Closed, reconnecting...");
      await this.setupChannel();
    });
    log.info("MQ Successfully Connnected");
  }

  async setupConn() {
    log.info("Connecting to MQ...");
    this.conn = await amqplib.connect(MQ_URI, "?heartbeat=60");
    this.setupChannel();
    log.info("MQ Successfully Set up");
  }

  publish(mq: string, data: string, hash: string) {
    if (!this.ch) {
      log.error("Could not publish data to MQ: `" + mq + "`, not connected.");
      return;
    }

    this.ch.assertQueue(mq, { durable: true });
    this.ch.sendToQueue(mq, Buffer.from(JSON.stringify({ data, hash })), {
      persistent: true,
    });
  }
}

const mq = new MQ();
export default mq;
