import bcrypt from "bcrypt";

import Admin from "../entity/Admin";

import { DEFAULT_ADMIN_PASSWORD, DEFAULT_ADMIN_USERNAME } from "../config";
import log from "../log";

const addDefaultAdmin = async () => {
  const admin = await Admin.findOne({});
  if (!admin) {
    log.info("Admin not found, pouring defaults");

    const hspw = await bcrypt.hash(DEFAULT_ADMIN_PASSWORD, 10);
    await Admin.create({
      username: DEFAULT_ADMIN_USERNAME,
      password: hspw,
    }).save();

    log.info("Default admin credentials saved");
  }
};

export default addDefaultAdmin;
