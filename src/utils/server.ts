import express, { Router } from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import multer from "multer";

import isAdmin from "../middleware/admin";
import textParser from "../middleware/text_parser";
import device_auth from "../middleware/device_auth";
import device_auth_without_pw from "../middleware/device_auth_without_pw";

import auth from "../admin/auth";
import bye from "../admin/bye";
import changepw from "../admin/change-pass";
import checkAuth from "../admin/check-auth";

import createGroup from "../admin/group/create";
import readGroup from "../admin/group/read";
import readSingleGroup from "../admin/group/read_single";
import updateGroup from "../admin/group/update";
import createFirmware from "../admin/group/firmware";
import makeFirmwareLatest from "../admin/group/firmware_latest";
import deleteFirmware from "../admin/group/firmware_delete";

import createDevice from "../admin/device/create";
import readDevice from "../admin/device/read";
import readSingleDevice from "../admin/group/read_single";
import updateDevice from "../admin/device/update";
import deleteDevice from "../admin/device/delete";
import disableDevice from "../admin/device/disable";
import enableDevice from "../admin/device/enable";

import createEvent from "../admin/event/create";
import readEvent from "../admin/event/read";
import readSingleEvent from "../admin/group/read_single";
import updateEvent from "../admin/event/update";
import deleteEvent from "../admin/event/delete";

import createActuation from "../admin/actuate/create";
import readActuation from "../admin/actuate/read";
import readSingleActuation from "../admin/actuate/read_single";
import updateActuation from "../admin/actuate/update";
import deleteActuation from "../admin/actuate/delete";

import data from "../admin/data";
import file from "../admin/file";

import push_text from "../api/push_text";
import push_json from "../api/push_json";
import push_file from "../api/push_file";
import firmware from "../api/firmware";

import checkDeviceExists from "../rpc/check-existence";
import actuateRpc from "../rpc/actuate";

import { FRONTEND_LOCATION } from "../config";

const app = express();
const upload = multer();

app.disable("x-powered-by");
app.disable("etag");

const adminRouter = Router();

adminRouter.use(
  cors({
    origin: FRONTEND_LOCATION,
    credentials: true,
  })
);

adminRouter.use(express.json());
adminRouter.use(cookieParser());

adminRouter.post("/auth", auth);
adminRouter.post("/bye", bye);

adminRouter.use(isAdmin);

adminRouter.get("/check-auth", checkAuth);
adminRouter.post("/change-pass", changepw);

adminRouter.post("/group", createGroup);
adminRouter.get("/group", readGroup);
adminRouter.get("/group/:id", readSingleGroup);
adminRouter.put("/group/:id", updateGroup);

adminRouter.post(
  "/group/:id/firmware",
  upload.single("firmware_bin"),
  createFirmware
);
adminRouter.post(
  "/group/:group_id/firmware/:firmware_id/make-latest",
  makeFirmwareLatest
);
adminRouter.delete("/group/:group_id/firmware/:firmware_id", deleteFirmware);

adminRouter.post("/device", createDevice);
adminRouter.get("/device", readDevice);
adminRouter.get("/device/:id", readSingleDevice);
adminRouter.put("/device/:id", updateDevice);
adminRouter.delete("/device/:id", deleteDevice);
adminRouter.post("/device/:id/disable", disableDevice);
adminRouter.post("/device/:id/enable", enableDevice);

adminRouter.post("/event", createEvent);
adminRouter.get("/event", readEvent);
adminRouter.get("/event/:id", readSingleEvent);
adminRouter.put("/event/:id", updateEvent);
adminRouter.delete("/event/:id", deleteEvent);

adminRouter.post("/actuate", createActuation);
adminRouter.get("/actuate", readActuation);
adminRouter.get("/actuate/:id", readSingleActuation);
adminRouter.put("/actuate/:id", updateActuation);
adminRouter.delete("/actuate/:id", deleteActuation);

adminRouter.get("/data", data);
adminRouter.post("/file", file);

app.use("/admin", adminRouter);

const rpcRouter = Router();

rpcRouter.use(express.json());

rpcRouter.get("/dev-exists", checkDeviceExists);
rpcRouter.post("/actuate", actuateRpc);

app.use("/rpc", rpcRouter);

app.post("/push/text", textParser, device_auth("text"), push_text);
app.post("/push/json", express.json(), device_auth("json"), push_json);
app.post("/push/file", upload.array("file"), device_auth("file"), push_file);
app.post("/firmware/:ver", device_auth_without_pw, firmware);

app.get("/healthcheck", (_, r) => r.sendStatus(200));

export default app;
