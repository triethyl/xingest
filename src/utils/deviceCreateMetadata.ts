import { v4 as uuidv4 } from "uuid";
import str from "@supercharge/strings";
import crypto from "crypto";

interface DeviceMetadata {
  device_id: string;
  device_password: string;
  device_hash: string;
}

const deviceCreateMetadata = (): DeviceMetadata => {
  const device_id = uuidv4();
  const device_password = str.random(50);

  const key = `${device_id}::${device_password}`;
  const device_hash = crypto.createHash("sha256").update(key).digest("hex");

  return {
    device_id,
    device_password,
    device_hash,
  };
};

export default deviceCreateMetadata;
