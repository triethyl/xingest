import { getConnection } from "typeorm";

import Device from "../entity/Device";
import Firmware from "../entity/Firmware";

const deviceFirmwareSave = async (
  groupId: string,
  deviceId: string,
  device_firmware_at_version: string
) => {
  // Check device firmware exists in db
  const device_firmware = await getConnection()
    .createQueryBuilder(Firmware, "firmware")
    .leftJoinAndSelect("firmware.group", "group")
    .where("firmware.version = :version", {
      version: device_firmware_at_version,
    })
    .andWhere("group.id = :groupId", { groupId })
    .getOne();

  if (device_firmware) {
    await getConnection()
      .createQueryBuilder(Device, "device")
      .update(Device)
      .set({ firmware_id: device_firmware.id })
      .where("id = :deviceId", { deviceId })
      .execute();
  }
};

export default deviceFirmwareSave;
