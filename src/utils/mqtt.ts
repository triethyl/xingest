import mqtt from "mqtt";
import { getConnection } from "typeorm";

import Event from "../entity/Event";
import Resolve, { TransportType } from "../entity/Resolve";

import mq from "./mq";
import { MQTT_URL } from "../config";

import Firmware from "../entity/Firmware";
import s3 from "./s3";
import deviceFirmwareSave from "./deviceFirmwareSave";

import log from "../log";

log.info("Connecting to MQTT...");

const mqttClient = mqtt.connect(MQTT_URL, {
  rejectUnauthorized: true,
});

const isValidJSON = (text: string): boolean => {
  try {
    JSON.parse(text);
    return true;
  } catch {
    return false;
  }
};

const onConnect = () => {
  mqttClient.subscribe("#");
  log.info("Connected to MQTT & subscribed to all data");
};

const onMessage: mqtt.OnMessageCallback = async (topic, payload) => {
  const r = /(?<platform>.*)\/(?<group>.*)\/(?<event>.*)\/(?<deviceId>.*)/g;
  const match = r.exec(topic);

  if (
    !match ||
    !match.groups ||
    Object.getOwnPropertyNames(match.groups).length !== 4
  )
    return;

  const groups = match.groups;

  const [devicePassword, message] = payload.toString().split("\n");
  const body = isValidJSON(message) ? "json" : "text";

  if (groups["event"] === "firmware-fetch") {
    const fm = await getConnection()
      .createQueryBuilder(Firmware, "firmware")
      .leftJoinAndSelect("firmware.group", "group")
      .leftJoinAndSelect("group.device", "device")
      .where("group.platform = :platform", { platform: groups["platform"] })
      .andWhere("group.name = :group", { group: groups["group"] })
      .andWhere("firmware.latest = :latest", { latest: true })
      .andWhere("device.deleted = :deviceDelete", { deviceDelete: false })
      .andWhere("device.disabled = :deviceDisable", { deviceDisable: false })
      .andWhere("device.device_id = :deviceId", {
        deviceId: groups["deviceId"],
      })
      .andWhere("device.device_password = :devicePassword", { devicePassword })
      .getOne();

    if (!fm) return;

    await deviceFirmwareSave(fm.group_id, fm.device[0].id, message);

    if (fm.version === message) return;

    const signed_url = await s3.getSignedUrlPromise("getObject", {
      Bucket: fm.s3_bucket,
      Key: fm.s3_key,
      Expires: 20000,
    });

    const topic = `${groups["platform"]}/${groups["group"]}/firmware-update/${groups["deviceId"]}`;
    mqttClient.publish(topic, signed_url);

    log.info("Device requested update", groups);

    return;
  }

  try {
    const ev = await getConnection()
      .createQueryBuilder(Event, "event")
      .leftJoinAndSelect("event.mq", "mq")
      .leftJoinAndSelect("event.group", "group")
      .leftJoinAndSelect("group.device", "device")
      .where("group.platform = :platform", { platform: groups["platform"] })
      .andWhere("group.name = :group", { group: groups["group"] })
      .andWhere("event.name = :event", { event: groups["event"] })
      .andWhere("event.body = :eventType", { eventType: body })
      .andWhere("event.deleted = :eventDelete", { eventDelete: false })
      .andWhere("device.deleted = :deviceDelete", { deviceDelete: false })
      .andWhere("device.disabled = :deviceDisable", { deviceDisable: false })
      .andWhere("device.device_id = :deviceId", {
        deviceId: groups["deviceId"],
      })
      .andWhere("device.device_password = :devicePassword", { devicePassword })
      .getOne();

    if (!ev) return;

    await Resolve.create({
      type: body,
      body: message || "",
      event_id: ev.id,
      group_id: ev.group_id,
      device_id: ev.group.device[0].id,
      transport: TransportType.mqtt,
    }).save();

    for (const { name } of ev.mq) {
      mq.publish(name, message || "", ev.group.device[0].device_hash);
    }
  } catch (e) {}
};

mqttClient.on("error", (e) => log.error(e));
mqttClient.on("connect", onConnect);
mqttClient.on("message", onMessage);

export default mqttClient;
