import { Response } from "express";

const customError = (
  res: Response,
  message: string,
  field: string,
  errorContext: string,
  value: any,
  returnErrorObject = false
) => {
  const data = {
    message,
    path: [field],
    type: `${field}.${errorContext}`,
    context: {
      value,
      label: field,
      key: field,
    },
  };
  if (returnErrorObject) return data;
  res.status(400).json([data]);
};

export default customError;
