import { createConnection } from "typeorm";

import {
  DATABASE_HOST,
  DATABASE_NAME,
  DATABASE_PASSWORD,
  DATABASE_USERNAME,
  NODE_ENV,
  REDIS_HOST,
  REDIS_PASSWORD,
  REDIS_PORT,
} from "../config";

import addDefaultAdmin from "./addDefaultAdmin";

import log from "../log";

const init = async () => {
  log.info("Trying to connect to database...");
  const con = await createConnection({
    type: "postgres",
    host: DATABASE_HOST,
    port: 5432,
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: DATABASE_NAME,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/*.ts"],
    synchronize: false,
    logging: NODE_ENV === "production",
    dropSchema: false,
    cache: {
      type: "redis",
      options: {
        host: REDIS_HOST,
        port: REDIS_PORT,
        password: REDIS_PASSWORD,
      },
    },
  });

  log.info("Running migrations...");
  await con.runMigrations();

  await addDefaultAdmin();
};

export default init;
