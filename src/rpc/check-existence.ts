import { Request, RequestHandler } from "express";

import Joi from "joi";

import Device from "../entity/Device";

interface RequestBody {
  device_hash: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    device_hash: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const checkDeviceExists: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { device_hash }: RequestBody = req.body;

  const dev = await Device.findOne({ device_hash });

  if (dev) res.status(200).json({ exists: true });
  else res.status(200).json({ exists: false });
};

export default checkDeviceExists;
