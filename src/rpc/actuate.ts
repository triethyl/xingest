import { Request, RequestHandler } from "express";

import Joi from "joi";
import mqttClient from "../utils/mqtt";

import Device from "../entity/Device";
import Actuate from "../entity/Actuate";
import Resolve, { TransportType } from "../entity/Resolve";

import customError from "../utils/customError";

interface RequestBody {
  device_hash: string;
  data: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    device_hash: Joi.string().required(),
    data: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const actuateRpc: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const auth = req.headers.authorization;
  if (!auth) return res.sendStatus(404);

  const actu = await Actuate.findOne({ auth });
  if (!actu)
    return customError(
      res,
      '"actuation" not found',
      "authorization",
      "not_found",
      auth
    );

  const { device_hash, data }: RequestBody = req.body;

  const dev = await Device.findOne({
    where: { device_hash },
    relations: ["group"],
  });

  if (!dev)
    return customError(
      res,
      '"device" not found',
      "device_hash",
      "not_found",
      device_hash
    );

  const topic = `${dev.group.platform}/${dev.group.name}/${actu.name}/${dev.device_id}`;
  mqttClient.publish(topic, data);

  await Resolve.create({
    type: "text",
    is_actuation: true,
    transport: TransportType.mqtt,
    body: data,
    actuation_id: actu.id,
    group_id: dev.group.id,
    device_id: dev.id,
  }).save();

  res.sendStatus(200);
};

export default actuateRpc;
