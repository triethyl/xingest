import { RequestHandler } from "express";
import Resolve from "../entity/Resolve";
import log from "../log";
import mq from "../utils/mq";

const push: RequestHandler = async (req, res) => {
  if (req.headers["content-type"] !== "application/json") {
    return res.sendStatus(400);
  }

  // All data is in string
  req.body = JSON.stringify(req.body);

  log.info(
    "JSON Data Sent from Device '" + req.source.deviceId + "': " + req.body,
    req.source
  );

  await Resolve.create({
    type: "json",
    body: req.body,
    event_id: req.source.eventRecordId,
    group_id: req.source.groupRecordId,
    device_id: req.source.deviceRecordId,
  }).save();

  for (const mq_name of req.source.mqs) {
    mq.publish(mq_name, req.body, req.source.deviceHash);
  }

  res.sendStatus(200);
};

export default push;
