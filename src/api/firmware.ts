import { RequestHandler } from "express";
import { getConnection } from "typeorm";

import Firmware from "../entity/Firmware";
import deviceFirmwareSave from "../utils/deviceFirmwareSave";
import s3 from "../utils/s3";

const firmware: RequestHandler = async (req, res) => {
  const device_firmware_at_version = req.params.ver;
  await deviceFirmwareSave(
    req.source.groupRecordId,
    req.source.deviceRecordId,
    device_firmware_at_version
  );

  // Fetch latest version
  const latest_firmware = await getConnection()
    .createQueryBuilder(Firmware, "firmware")
    .leftJoinAndSelect("firmware.group", "group")
    .where("firmware.latest = :latest", { latest: true })
    .andWhere("group.id = :groupId", { groupId: req.source.groupRecordId })
    .getOne();

  if (!latest_firmware) return res.sendStatus(400);
  if (device_firmware_at_version === latest_firmware.version)
    return res.sendStatus(400);

  // Update is available
  const signed_url = await s3.getSignedUrlPromise("getObject", {
    Bucket: latest_firmware.s3_bucket,
    Key: latest_firmware.s3_key,
    Expires: 20000,
  });

  res.status(200).send(signed_url);
};

export default firmware;
