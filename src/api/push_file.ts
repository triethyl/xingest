import { RequestHandler } from "express";
import { v4 as uuidv4 } from "uuid";

import Resolve from "../entity/Resolve";

import mq from "../utils/mq";
import s3 from "../utils/s3";

import log from "../log";

import { AWS_BUCKET_NAME } from "../config";

const push: RequestHandler = async (req, res) => {
  // Enforcing array req.files
  req.files = Array.isArray(req.files) ? req.files : [];

  if (
    !req.headers["content-type"] ||
    !req.headers["content-type"].startsWith("multipart/form-data")
  ) {
    return res.sendStatus(400);
  }

  if (req.files.length === 0) {
    return res.sendStatus(400);
  }

  if (!req.source.eventAllowMultipleFiles && req.files.length > 1) {
    return res.sendStatus(400);
  }

  for (const f of req.files) {
    if (f.mimetype !== req.source.eventFileMimetype) {
      return res.sendStatus(400);
    }
  }

  // All cool, upload to s3 and resolve

  log.info(
    "File Data Sent from Device '" + req.source.deviceId + "'",
    req.source
  );

  const files: { key: string; bucket: string }[] = [];

  for (const f of req.files) {
    const key = `FILE-FROM-DEVICE-${uuidv4()}`;
    await s3
      .upload({
        Bucket: AWS_BUCKET_NAME,
        Key: key,
        Body: f.buffer,
        ContentType: f.mimetype,
      })
      .promise();

    files.push({ key, bucket: AWS_BUCKET_NAME });
  }

  const fileData = JSON.stringify(files);

  await Resolve.create({
    type: "file",
    body: fileData,
    event_id: req.source.eventRecordId,
    group_id: req.source.groupRecordId,
    device_id: req.source.deviceRecordId,
  }).save();

  for (const mq_name of req.source.mqs) {
    mq.publish(mq_name, fileData, req.source.deviceHash);
  }

  res.sendStatus(200);
};

export default push;
