import { RequestHandler } from "express";
import Resolve from "../entity/Resolve";
import log from "../log";
import mq from "../utils/mq";

const push: RequestHandler = async (req, res) => {
  if (req.headers["content-type"] !== "text/plain") {
    return res.sendStatus(400);
  }

  // Just to be safe
  req.text = String(req.text);

  log.info(
    "Text Data Sent from Device '" + req.source.deviceId + "': " + req.text,
    req.source
  );

  await Resolve.create({
    type: "text",
    body: req.text,
    event_id: req.source.eventRecordId,
    group_id: req.source.groupRecordId,
    device_id: req.source.deviceRecordId,
  }).save();

  for (const mq_name of req.source.mqs) {
    mq.publish(mq_name, req.text, req.source.deviceHash);
  }

  res.sendStatus(200);
};

export default push;
