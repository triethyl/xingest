/* Application level configuration objects to be exported
 * from here
 */

import dotenv from "dotenv";
dotenv.config();

export const {
  // App
  NODE_ENV = "development",
  FRONTEND_LOCATION = "https://xingest.durbinservices.com",

  // Default Admin Settings
  DEFAULT_ADMIN_USERNAME = "admin",
  DEFAULT_ADMIN_PASSWORD = "password",

  // Cookie Settings
  JWT_SIGNKEY = "jwt-sign-key",
  JWT_EXPIRESIN = "5m",

  // AWS
  AWS_ACCESS_KEY = "",
  AWS_SECRET_ACCESS_KEY = "",
  AWS_BUCKET_NAME = "",
  AWS_BUCKET_REGION = "ap-south-1",

  // Postgres Database
  DATABASE_TYPE = "postgres",
  DATABASE_HOST = "localhost",
  DATABASE_PORT = 5432,
  DATABASE_USERNAME = "postgres",
  DATABASE_PASSWORD = "",
  DATABASE_NAME = "xingest",

  // Redis
  REDIS_HOST = "localhost",
  REDIS_PASSWORD = "password",

  // RabbitMQ
  MQ_USERNAME = "username",
  MQ_PASSWORD = "password",
  MQ_HOST = "localhost",
  MQ_VHOST = "",

  // MQTT
  MQTT_URL = "",
} = process.env;

export const APP_PORT = parseInt(process.env.APP_PORT || "4000");
export const REDIS_PORT = parseInt(process.env.REDIS_PORT || "6379");
export const MQ_PORT = parseInt(process.env.MQ_PORT || "5672");
export const MQ_URI =
  process.env.MQ_URI ||
  `amqp://${MQ_USERNAME}:${MQ_PASSWORD}@${MQ_HOST}:${MQ_PORT}/${MQ_VHOST}`;
