import { MigrationInterface, QueryRunner } from "typeorm";

export class UniqueConstraints1615397232862 implements MigrationInterface {
  name = "UniqueConstraints1615397232862";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "UQ_93ddc8b51de961eb2c6040603f7" UNIQUE ("version", "group_id")`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD CONSTRAINT "UQ_859bc661e7ed090ff0a60dea833" UNIQUE ("platform", "name")`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device_group" DROP CONSTRAINT "UQ_859bc661e7ed090ff0a60dea833"`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "UQ_93ddc8b51de961eb2c6040603f7"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
  }
}
