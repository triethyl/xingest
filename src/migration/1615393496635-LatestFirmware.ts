import { MigrationInterface, QueryRunner } from "typeorm";

export class LatestFirmware1615393496635 implements MigrationInterface {
  name = "LatestFirmware1615393496635";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "firmware" RENAME COLUMN "draft" TO "latest"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "firmware"."latest" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ALTER COLUMN "latest" SET DEFAULT false`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" ALTER COLUMN "latest" SET DEFAULT true`
    );
    await queryRunner.query(`COMMENT ON COLUMN "firmware"."latest" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "firmware" RENAME COLUMN "latest" TO "draft"`
    );
  }
}
