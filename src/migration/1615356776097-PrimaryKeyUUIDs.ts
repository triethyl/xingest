import { MigrationInterface, QueryRunner } from "typeorm";

export class PrimaryKeyUUIDs1615356776097 implements MigrationInterface {
  name = "PrimaryKeyUUIDs1615356776097";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "mq" DROP CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2"`
    );
    await queryRunner.query(
      `ALTER TABLE "mq" DROP CONSTRAINT "PK_6ecb7f2871ebb85b51e5cf839f5"`
    );
    await queryRunner.query(`ALTER TABLE "mq" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "mq" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`
    );
    await queryRunner.query(
      `ALTER TABLE "mq" ADD CONSTRAINT "PK_6ecb7f2871ebb85b51e5cf839f5" PRIMARY KEY ("id")`
    );
    await queryRunner.query(`ALTER TABLE "mq" DROP COLUMN "event_id"`);
    await queryRunner.query(`ALTER TABLE "mq" ADD "event_id" uuid NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806"`
    );
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614"`
    );
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "event" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614" PRIMARY KEY ("id")`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "group_id"`);
    await queryRunner.query(`ALTER TABLE "event" ADD "group_id" uuid NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d"`
    );
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1"`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" DROP CONSTRAINT "PK_6bb808be579ff0722c914a8d6a1"`
    );
    await queryRunner.query(`ALTER TABLE "device_group" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD CONSTRAINT "PK_6bb808be579ff0722c914a8d6a1" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5"`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "PK_7c4dbd3acc67ba8c6de2ef94737"`
    );
    await queryRunner.query(`ALTER TABLE "firmware" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "PK_7c4dbd3acc67ba8c6de2ef94737" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`ALTER TABLE "firmware" DROP COLUMN "group_id"`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD "group_id" uuid NOT NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72"`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72" PRIMARY KEY ("id")`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "firmware_id"`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD "firmware_id" uuid NOT NULL`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "group_id"`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD "group_id" uuid NOT NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "mq" ADD CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5" FOREIGN KEY ("firmware_id") REFERENCES "firmware"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1"`
    );
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5"`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d"`
    );
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806"`
    );
    await queryRunner.query(
      `ALTER TABLE "mq" DROP CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2"`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "group_id"`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD "group_id" integer NOT NULL`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "firmware_id"`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD "firmware_id" integer NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72"`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "device" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72" PRIMARY KEY ("id")`
    );
    await queryRunner.query(`ALTER TABLE "firmware" DROP COLUMN "group_id"`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD "group_id" integer NOT NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "PK_7c4dbd3acc67ba8c6de2ef94737"`
    );
    await queryRunner.query(`ALTER TABLE "firmware" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "firmware" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "PK_7c4dbd3acc67ba8c6de2ef94737" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5" FOREIGN KEY ("firmware_id") REFERENCES "firmware"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" DROP CONSTRAINT "PK_6bb808be579ff0722c914a8d6a1"`
    );
    await queryRunner.query(`ALTER TABLE "device_group" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD "id" SERIAL NOT NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD CONSTRAINT "PK_6bb808be579ff0722c914a8d6a1" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "group_id"`);
    await queryRunner.query(
      `ALTER TABLE "event" ADD "group_id" integer NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614"`
    );
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "event" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(`ALTER TABLE "mq" DROP COLUMN "event_id"`);
    await queryRunner.query(`ALTER TABLE "mq" ADD "event_id" integer NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "mq" DROP CONSTRAINT "PK_6ecb7f2871ebb85b51e5cf839f5"`
    );
    await queryRunner.query(`ALTER TABLE "mq" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "mq" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "mq" ADD CONSTRAINT "PK_6ecb7f2871ebb85b51e5cf839f5" PRIMARY KEY ("id")`
    );
    await queryRunner.query(
      `ALTER TABLE "mq" ADD CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }
}
