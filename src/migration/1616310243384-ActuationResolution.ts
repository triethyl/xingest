import { MigrationInterface, QueryRunner } from "typeorm";

export class ActuationResolution1616310243384 implements MigrationInterface {
  name = "ActuationResolution1616310243384";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD "is_actuation" boolean NOT NULL DEFAULT false`
    );
    await queryRunner.query(
      `CREATE TYPE "resolve_transport_enum" AS ENUM('http', 'mqtt')`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD "transport" "resolve_transport_enum" NOT NULL DEFAULT 'http'`
    );
    await queryRunner.query(`ALTER TABLE "resolve" ADD "actuation_id" uuid`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "resolve" ALTER COLUMN "event_id" DROP NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."event_id" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_c4ac1eaa6e514520f116884a4d7" FOREIGN KEY ("actuation_id") REFERENCES "actuate"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_c4ac1eaa6e514520f116884a4d7"`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616"`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."event_id" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "resolve" ALTER COLUMN "event_id" SET NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`ALTER TABLE "resolve" DROP COLUMN "actuation_id"`);
    await queryRunner.query(`ALTER TABLE "resolve" DROP COLUMN "transport"`);
    await queryRunner.query(`DROP TYPE "resolve_transport_enum"`);
    await queryRunner.query(`ALTER TABLE "resolve" DROP COLUMN "is_actuation"`);
  }
}
