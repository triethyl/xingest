import { MigrationInterface, QueryRunner } from "typeorm";

export class EventBodyEnumFix1615378547119 implements MigrationInterface {
  name = "EventBodyEnumFix1615378547119";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TYPE "public"."event_body_enum" RENAME TO "event_body_enum_old"`
    );
    await queryRunner.query(
      `CREATE TYPE "event_body_enum" AS ENUM('text', 'json', 'file')`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ALTER COLUMN "body" TYPE "event_body_enum" USING "body"::"text"::"event_body_enum"`
    );
    await queryRunner.query(`DROP TYPE "event_body_enum_old"`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."body" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."body" IS NULL`);
    await queryRunner.query(
      `CREATE TYPE "event_body_enum_old" AS ENUM('0', '1', '2')`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ALTER COLUMN "body" TYPE "event_body_enum_old" USING "body"::"text"::"event_body_enum_old"`
    );
    await queryRunner.query(`DROP TYPE "event_body_enum"`);
    await queryRunner.query(
      `ALTER TYPE "event_body_enum_old" RENAME TO  "event_body_enum"`
    );
  }
}
