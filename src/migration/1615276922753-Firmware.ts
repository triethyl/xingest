import { MigrationInterface, QueryRunner } from "typeorm";

export class Firmware1615276922753 implements MigrationInterface {
  name = "Firmware1615276922753";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "firmware" ("id" SERIAL NOT NULL, "version" character varying NOT NULL, "s3_bucket" character varying NOT NULL, "s3_key" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "group_id" integer NOT NULL, CONSTRAINT "PK_7c4dbd3acc67ba8c6de2ef94737" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`
    );
    await queryRunner.query(
      `ALTER TABLE "device_group" ADD "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP`
    );
    await queryRunner.query(
      `ALTER TABLE "firmware" ADD CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "firmware" DROP CONSTRAINT "FK_7d597b1dc3809fa0bd9bef8018d"`
    );
    await queryRunner.query(`ALTER TABLE "device" DROP COLUMN "created_at"`);
    await queryRunner.query(
      `ALTER TABLE "device_group" DROP COLUMN "created_at"`
    );
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "created_at"`);
    await queryRunner.query(`DROP TABLE "firmware"`);
  }
}
