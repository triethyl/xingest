import { MigrationInterface, QueryRunner } from "typeorm";

export class ActuateDeleteDefault1616277460225 implements MigrationInterface {
  name = "ActuateDeleteDefault1616277460225";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "actuate"."deleted" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "actuate" ALTER COLUMN "deleted" SET DEFAULT false`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "actuate" ALTER COLUMN "deleted" DROP DEFAULT`
    );
    await queryRunner.query(`COMMENT ON COLUMN "actuate"."deleted" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
  }
}
