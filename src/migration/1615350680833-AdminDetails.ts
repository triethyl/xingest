import { MigrationInterface, QueryRunner } from "typeorm";

export class AdminDetails1615350680833 implements MigrationInterface {
  name = "AdminDetails1615350680833";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "admin" ("id" SERIAL NOT NULL, "username" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "PK_e032310bcef831fb83101899b10" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`DROP TABLE "admin"`);
  }
}
