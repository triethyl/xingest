import { MigrationInterface, QueryRunner } from "typeorm";

export class Device1615274246144 implements MigrationInterface {
  name = "Device1615274246144";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "device" ("id" SERIAL NOT NULL, "device_id" character varying NOT NULL, "device_password" character varying NOT NULL, "device_hash" character varying NOT NULL, "for_company" character varying NOT NULL, "tags" character varying NOT NULL, "group_id" integer NOT NULL, CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_6bb808be579ff0722c914a8d6a1"`
    );
    await queryRunner.query(`DROP TABLE "device"`);
  }
}
