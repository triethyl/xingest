import { MigrationInterface, QueryRunner } from "typeorm";

export class DeviceGroup1615270170718 implements MigrationInterface {
  name = "DeviceGroup1615270170718";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "device_group" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "purpose" character varying NOT NULL, "platform" character varying NOT NULL, CONSTRAINT "PK_6bb808be579ff0722c914a8d6a1" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD "group_id" integer NOT NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "FK_cc0c9c66eb7ef8bb3d8d5164806"`
    );
    await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "group_id"`);
    await queryRunner.query(`DROP TABLE "device_group"`);
  }
}
