import { MigrationInterface, QueryRunner } from "typeorm";

export class DeviceFirmwareNullable1615359086243 implements MigrationInterface {
  name = "DeviceFirmwareNullable1615359086243";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" ALTER COLUMN "firmware_id" DROP NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."firmware_id" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5" FOREIGN KEY ("firmware_id") REFERENCES "firmware"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "device" DROP CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."firmware_id" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" ALTER COLUMN "firmware_id" SET NOT NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "device" ADD CONSTRAINT "FK_b25ad78b6309a8b4e8181a8cec5" FOREIGN KEY ("firmware_id") REFERENCES "firmware"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
  }
}
