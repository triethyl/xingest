import { MigrationInterface, QueryRunner } from "typeorm";

export class DataResolution1615448305331 implements MigrationInterface {
  name = "DataResolution1615448305331";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "resolve" ("id" SERIAL NOT NULL, "type" character varying NOT NULL, "body" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "event_id" uuid NOT NULL, "group_id" uuid NOT NULL, "device_id" uuid NOT NULL, CONSTRAINT "PK_8c1cad3809aefff21253adecaf1" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_93fa1dad155530d6f08accce38d" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" ADD CONSTRAINT "FK_f23566eba8f2b9ca2a02af6cb77" FOREIGN KEY ("device_id") REFERENCES "device"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_f23566eba8f2b9ca2a02af6cb77"`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_93fa1dad155530d6f08accce38d"`
    );
    await queryRunner.query(
      `ALTER TABLE "resolve" DROP CONSTRAINT "FK_9c37f1162f89e3fe29a9bf84616"`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`DROP TABLE "resolve"`);
  }
}
