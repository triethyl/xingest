import { MigrationInterface, QueryRunner } from "typeorm";

export class ActuateDelete1616272308602 implements MigrationInterface {
  name = "ActuateDelete1616272308602";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "actuate" ADD "deleted" boolean NOT NULL`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`ALTER TABLE "actuate" DROP COLUMN "deleted"`);
  }
}
