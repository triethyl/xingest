import { MigrationInterface, QueryRunner } from "typeorm";

export class Actuate1616269642145 implements MigrationInterface {
  name = "Actuate1616269642145";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "actuate" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "auth" character varying NOT NULL, "group_id" uuid NOT NULL, CONSTRAINT "PK_1a5587cb31fb1582ecd45e2fc9e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(
      `ALTER TABLE "actuate" ADD CONSTRAINT "FK_1a662dd49346c65e6bee98e19f5" FOREIGN KEY ("group_id") REFERENCES "device_group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "actuate" DROP CONSTRAINT "FK_1a662dd49346c65e6bee98e19f5"`
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "device_group"."created_at" IS NULL`
    );
    await queryRunner.query(`COMMENT ON COLUMN "device"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "resolve"."created_at" IS NULL`);
    await queryRunner.query(`COMMENT ON COLUMN "event"."created_at" IS NULL`);
    await queryRunner.query(
      `COMMENT ON COLUMN "firmware"."created_at" IS NULL`
    );
    await queryRunner.query(`DROP TABLE "actuate"`);
  }
}
