import { MigrationInterface, QueryRunner } from "typeorm";

export class Event1615269027580 implements MigrationInterface {
  name = "Event1615269027580";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "event_body_enum" AS ENUM('0', '1', '2')`
    );
    await queryRunner.query(
      `CREATE TABLE "event" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "event_id" character varying NOT NULL, "body" "event_body_enum" NOT NULL, "file_mimetype" character varying, "file_multiple" boolean, CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(`ALTER TABLE "mq" ADD "event_id" integer NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "mq" ADD CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2" FOREIGN KEY ("event_id") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "mq" DROP CONSTRAINT "FK_a07ee8c7446d90c513bd4c1eda2"`
    );
    await queryRunner.query(`ALTER TABLE "mq" DROP COLUMN "event_id"`);
    await queryRunner.query(`DROP TABLE "event"`);
    await queryRunner.query(`DROP TYPE "event_body_enum"`);
  }
}
