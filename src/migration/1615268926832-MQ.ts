import { MigrationInterface, QueryRunner } from "typeorm";

export class MQ1615268926832 implements MigrationInterface {
  name = "MQ1615268926832";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "mq" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_6ecb7f2871ebb85b51e5cf839f5" PRIMARY KEY ("id"))`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "mq"`);
  }
}
