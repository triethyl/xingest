import { RequestHandler } from "express";

const checkAuth: RequestHandler = async (_, res) => {
  res.sendStatus(200);
};

export default checkAuth;
