import { Request, RequestHandler } from "express";

import Joi from "joi";

import MQ from "../../entity/MQ";
import DeviceGroup from "../../entity/DeviceGroup";
import Event, { EventBody } from "../../entity/Event";

import customError from "../../utils/customError";
import log from "../../log";

interface RequestBody {
  group_id?: string;
  name?: string;
  body?: EventBody;
  file_mimetype?: string;
  file_multiple?: boolean;
  mqs?: string[];
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }),
    name: Joi.string()
      .regex(/^[a-z0-9-]+$/)
      .invalid("firmware-fetch"),
    body: Joi.string().valid("text", "json", "file"),
    file_mimetype: Joi.string(),
    file_multiple: Joi.boolean(),
    mqs: Joi.array().items(Joi.string()),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const updateEvent: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const event = await Event.findOne({ id: req.params.id, deleted: false });
  if (!event)
    return customError(
      res,
      '"event" not found',
      "id",
      "not_found",
      req.params.id
    );

  const {
    group_id,
    name,
    body,
    file_mimetype,
    file_multiple,
    mqs,
  }: RequestBody = req.body;

  if (group_id) {
    const group = await DeviceGroup.findOne({ id: group_id });
    if (!group)
      return customError(res, '"group" not found', "id", "not_found", group_id);

    event.group_id = group_id;
  }

  if (body === "file") {
    // Need these params if file
    const errors = [];
    if (typeof file_mimetype === "undefined")
      errors.push(
        customError(
          res,
          '"file_mimetype" is required if body is "file"',
          "file_mimetype",
          "not_given",
          file_mimetype,
          true
        )
      );
    if (typeof file_multiple === "undefined")
      errors.push(
        customError(
          res,
          '"file_multiple" is required if body is "file"',
          "file_multiple",
          "not_given",
          file_multiple,
          true
        )
      );
    if (errors.length > 0) return res.status(400).json(errors);

    event.file_mimetype = file_mimetype;
    event.file_multiple = file_multiple;
  } else if (body === "text" || body === "json") {
    event.file_mimetype = null;
    event.file_multiple = null;
  }

  if (name) event.name = name;
  if (body) event.body = body;

  if (mqs) {
    const existing_mqs = (await MQ.find({ event_id: event.id })).map(
      (m) => m.name
    );

    const mqs_to_create = [];
    const mqs_to_delete = [];

    for (const mq of mqs)
      if (!existing_mqs.includes(mq)) mqs_to_create.push(mq);

    for (const emq of existing_mqs)
      if (!mqs.includes(emq)) mqs_to_delete.push(emq);

    for (const mq of mqs_to_create)
      await MQ.create({ name: mq, event_id: event.id }).save();

    for (const mq of mqs_to_delete) {
      const m = await MQ.findOne({ event_id: event.id, name: mq });
      if (m) await m.remove();
    }
  }

  await event.save();

  res.sendStatus(200);
  log.info("Event updated successfully", event);
};

export default updateEvent;
