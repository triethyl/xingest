import { RequestHandler } from "express";

import Event from "../../entity/Event";
import customError from "../../utils/customError";

import log from "../../log";

const deleteEvent: RequestHandler = async (req, res) => {
  const event = await Event.findOne({ id: req.params.id, deleted: false });
  if (!event)
    return customError(
      res,
      '"event" not found',
      "id",
      "not_found",
      req.params.id
    );

  event.deleted = true;
  await event.save();

  res.sendStatus(200);
  log.info("Event deleted successfully", event);
};

export default deleteEvent;
