import { RequestHandler } from "express";

import Event from "../../entity/Event";

const readSingleEvent: RequestHandler = async (req, res) => {
  let data: Event[];

  let deleted = false;
  if (req.query.showDeleted) deleted = true;

  if (req.query.full)
    data = await Event.find({
      where: { id: req.params.id, deleted },
      relations: ["group", "group.device", "mq"],
    });
  else data = await Event.find({ id: req.params.id, deleted });

  res.json(data);
};

export default readSingleEvent;
