import { Request, RequestHandler } from "express";

import Joi from "joi";

import MQ from "../../entity/MQ";
import DeviceGroup from "../../entity/DeviceGroup";
import Event, { EventBody } from "../../entity/Event";

import customError from "../../utils/customError";
import log from "../../log";

interface RequestBody {
  group_id: string;
  name: string;
  body: EventBody;
  file_mimetype?: string;
  file_multiple?: boolean;
  mqs: string[];
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }).required(),
    name: Joi.string()
      .regex(/[a-z0-9-]+/)
      .invalid("firmware-fetch")
      .required(),
    body: Joi.string().valid("text", "json", "file").required(),
    file_mimetype: Joi.string(),
    file_multiple: Joi.boolean(),
    mqs: Joi.array()
      .items(Joi.string().regex(/^[a-z0-9-]+$/))
      .required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const createEvent: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const {
    group_id,
    name,
    body,
    file_mimetype,
    file_multiple,
    mqs,
  }: RequestBody = req.body;

  const group = await DeviceGroup.findOne({ id: group_id });
  if (!group)
    return customError(res, '"group" not found', "id", "not_found", group_id);

  if (body === "file") {
    // Need these params if file
    const errors = [];
    if (typeof file_mimetype === "undefined")
      errors.push(
        customError(
          res,
          '"file_mimetype" is required if body is "file"',
          "file_mimetype",
          "not_given",
          file_mimetype,
          true
        )
      );
    if (typeof file_multiple === "undefined")
      errors.push(
        customError(
          res,
          '"file_multiple" is required if body is "file"',
          "file_multiple",
          "not_given",
          file_multiple,
          true
        )
      );
    if (errors.length > 0) return res.status(400).json(errors);

    const event = await Event.create({
      name,
      body,
      group_id,
      file_mimetype,
      file_multiple,
    }).save();

    for (const mq of mqs)
      await MQ.create({ name: mq, event_id: event.id }).save();

    res.sendStatus(200);

    log.info("Event created successfully", event);
  } else {
    const event = await Event.create({
      name,
      body,
      group_id,
    }).save();

    for (const mq of mqs)
      await MQ.create({ name: mq, event_id: event.id }).save();

    res.sendStatus(200);

    log.info("Event created successfully", event);
  }
};

export default createEvent;
