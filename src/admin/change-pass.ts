import { Request, RequestHandler } from "express";

import bcrypt from "bcrypt";
import Joi from "joi";

import Admin from "../entity/Admin";

import { DEFAULT_ADMIN_USERNAME } from "../config";

import customError from "../utils/customError";

import log from "../log";

interface RequestBody {
  oldPassword: string;
  newPassword: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().min(8).required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const changepw: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { oldPassword, newPassword }: RequestBody = req.body;

  try {
    const rx = await Admin.findOne({ username: DEFAULT_ADMIN_USERNAME });

    if (!rx) throw Error;
    if (!(await bcrypt.compare(oldPassword, rx.password))) throw Error;

    rx.password = await bcrypt.hash(newPassword, 10);

    await rx.save();

    res.sendStatus(200);
    log.info("Admin changed password");
  } catch {
    res
      .status(400)
      .json([
        customError(
          res,
          '"oldPassword" incorrect',
          "oldPassword",
          "incorrect",
          oldPassword,
          true
        ),
        customError(
          res,
          '"newPassword" incorrect',
          "newPassword",
          "incorrect",
          newPassword,
          true
        ),
      ]);

    log.info("Admin change password failed");
  }
};

export default changepw;
