import { Request, RequestHandler } from "express";

import Joi from "joi";

import DeviceGroup from "../../entity/DeviceGroup";
import customError from "../../utils/customError";

import log from "../../log";

interface RequestBody {
  name?: string;
  purpose?: string;
  platform?: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    name: Joi.string().regex(/^[a-z0-9-]+$/),
    purpose: Joi.string(),
    platform: Joi.string().regex(/^[a-z0-9-]+$/),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const updateGroup: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const group = await DeviceGroup.findOne({ id: req.params.id });
  if (!group)
    return customError(
      res,
      '"group" not found',
      "id",
      "not_found",
      req.params.id
    );

  const { name, purpose, platform }: RequestBody = req.body;

  if (name) group.name = name;
  if (purpose) group.purpose = purpose;
  if (platform) group.platform = platform;

  await group.save();

  res.sendStatus(200);
  log.info("Group updated successfully", group);
};

export default updateGroup;
