import { RequestHandler } from "express";

import DeviceGroup from "../../entity/DeviceGroup";

const readGroup: RequestHandler = async (req, res) => {
  let data: DeviceGroup[];

  if (req.query.full)
    data = await DeviceGroup.find({
      where: {},
      relations: ["device", "event", "firmware"],
    });
  else data = await DeviceGroup.find({});

  res.json(data);
};

export default readGroup;
