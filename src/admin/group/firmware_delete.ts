import { RequestHandler } from "express";

import Firmware from "../../entity/Firmware";

import customError from "../../utils/customError";
import s3 from "../../utils/s3";

const deleteFirmware: RequestHandler = async (req, res) => {
  const group_id = req.params.group_id;
  const firmware_id = req.params.firmware_id;

  const firmware = await Firmware.findOne({
    group_id: group_id,
    id: firmware_id,
    latest: false,
  });

  if (!firmware)
    return customError(
      res,
      '"firmware" not found or is latest',
      "firmware_id",
      "not_found",
      firmware_id
    );

  await s3
    .deleteObject({ Bucket: firmware.s3_bucket, Key: firmware.s3_key })
    .promise();

  await firmware.remove();

  res.sendStatus(200);
};

export default deleteFirmware;
