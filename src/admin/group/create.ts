import { Request, RequestHandler } from "express";

import Joi from "joi";

import DeviceGroup from "../../entity/DeviceGroup";
import log from "../../log";

interface RequestBody {
  name: string;
  purpose: string;
  platform: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    name: Joi.string()
      .regex(/^[a-z0-9-]+$/)
      .required(),
    purpose: Joi.string().required(),
    platform: Joi.string()
      .regex(/^[a-z0-9-]+$/)
      .required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const createGroup: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { name, purpose, platform }: RequestBody = req.body;

  const group = await DeviceGroup.create({
    name,
    purpose,
    platform,
  }).save();

  res.sendStatus(200);
  log.info("Group created successfully", group);
};

export default createGroup;
