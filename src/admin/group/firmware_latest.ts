import { RequestHandler } from "express";
import DeviceGroup from "../../entity/DeviceGroup";

import Firmware from "../../entity/Firmware";

import customError from "../../utils/customError";
import mqttClient from "../../utils/mqtt";
import s3 from "../../utils/s3";

const makeFirmwareLatest: RequestHandler = async (req, res) => {
  const group_id = req.params.group_id;
  const firmware_id = req.params.firmware_id;

  const firmware = await Firmware.findOne({
    group_id: group_id,
    id: firmware_id,
  });

  if (!firmware)
    return customError(
      res,
      '"firmware" not found',
      "firmware_id",
      "not_found",
      firmware_id
    );

  const group = await DeviceGroup.findOne({
    where: { id: group_id },
    relations: ["firmware", "device"],
  });

  if (!group)
    return customError(res, '"group" not found', "id", "not_found", group_id);

  const firmwareDetails = { Bucket: "", Key: "" };

  for (const f of group.firmware) {
    if (f.id === firmware_id) {
      f.latest = true;

      firmwareDetails.Bucket = f.s3_bucket;
      firmwareDetails.Key = f.s3_key;
    } else f.latest = false;
    await f.save();
  }

  const { Bucket, Key } = firmwareDetails;

  if (!Bucket || !Key)
    return customError(
      res,
      '"firmware" not found',
      "firmware_id",
      "not_found",
      firmware_id
    );

  // Sign URL for this firmware
  const signed_url = await s3.getSignedUrlPromise("getObject", {
    Bucket,
    Key,
    Expires: 20000,
  });

  // Publish firmware update request to everyone
  for (const { device_id } of group.device) {
    const topic = `${group.platform}/${group.name}/firmware-update/${device_id}`;
    mqttClient.publish(topic, signed_url);
  }

  res.sendStatus(200);
};

export default makeFirmwareLatest;
