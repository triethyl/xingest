import { RequestHandler } from "express";

import DeviceGroup from "../../entity/DeviceGroup";

const readSingleGroup: RequestHandler = async (req, res) => {
  let data: DeviceGroup[];

  if (req.query.full)
    data = await DeviceGroup.find({
      where: { id: req.params.id },
      relations: ["device", "event", "firmware"],
    });
  else data = await DeviceGroup.find({ id: req.params.id });

  res.json(data);
};

export default readSingleGroup;
