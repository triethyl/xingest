import { Request, RequestHandler } from "express";
import { v4 as uuidv4 } from "uuid";
import Joi from "joi";

import DeviceGroup from "../../entity/DeviceGroup";

import customError from "../../utils/customError";
import s3 from "../../utils/s3";

import { AWS_BUCKET_NAME } from "../../config";
import Firmware from "../../entity/Firmware";

interface RequestBody {
  version: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    version: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const createFirmware: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  if (!req.file.mimetype || req.file.mimetype !== "application/octet-stream")
    return customError(
      res,
      "Binary's Mimetype invalid",
      "file",
      "mimetype_invalid",
      req.file.mimetype
    );

  const group = await DeviceGroup.findOne({ id: req.params.id });
  if (!group)
    return customError(
      res,
      '"group" not found',
      "id",
      "not_found",
      req.params.id
    );

  const { version }: RequestBody = req.body;

  const key = `FIRMWARE-${uuidv4()}.bin`;

  await s3
    .upload({
      Bucket: AWS_BUCKET_NAME,
      Key: key,
      Body: req.file.buffer,
      ContentType: req.file.mimetype,
    })
    .promise();

  await Firmware.create({
    group_id: group.id,
    version,
    s3_bucket: AWS_BUCKET_NAME,
    s3_key: key,
  }).save();

  res.sendStatus(200);
};

export default createFirmware;
