import { RequestHandler } from "express";

import log from "../log";

const bye: RequestHandler = async (_, res) => {
  res.clearCookie("uid");
  res.sendStatus(200);
  log.info("Admin logout");
};

export default bye;
