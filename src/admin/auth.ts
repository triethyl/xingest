import { Request, RequestHandler } from "express";

import bcrypt from "bcrypt";
import Joi from "joi";
import jwt from "jsonwebtoken";

import Admin from "../entity/Admin";

import { JWT_EXPIRESIN, JWT_SIGNKEY } from "../config";

import customError from "../utils/customError";

import log from "../log";

interface RequestBody {
  username: string;
  password: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const auth: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { username, password }: RequestBody = req.body;

  try {
    const rx = await Admin.findOne({ username });

    if (!rx) throw Error;
    if (!(await bcrypt.compare(password, rx.password))) throw Error;

    res.cookie(
      "uid",
      jwt.sign({ username }, JWT_SIGNKEY, { expiresIn: JWT_EXPIRESIN }),
      { httpOnly: true }
    );
    res.sendStatus(200);

    log.info("Admin login succeded");
  } catch {
    res
      .status(400)
      .json([
        customError(
          res,
          '"username" or "password" incorrect',
          "username",
          "incorrect",
          username,
          true
        ),
        customError(
          res,
          '"password" or "password" incorrect',
          "password",
          "incorrect",
          password,
          true
        ),
      ]);

    log.info("Admin login failed");
  }
};

export default auth;
