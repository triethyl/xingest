import { RequestHandler } from "express";

import Resolve from "../entity/Resolve";

interface WhereClause {
  device_id?: string;
  group_id?: string;
  event_id?: string;
  actuation_id?: string;
}

const data: RequestHandler = async (req, res) => {
  try {
    const where_clause: WhereClause = {};

    if (req.query.device)
      where_clause["device_id"] = req.query.device as string;
    if (req.query.group) where_clause["group_id"] = req.query.group as string;
    if (req.query.event) where_clause["event_id"] = req.query.event as string;
    if (req.query.actuation)
      where_clause["actuation_id"] = req.query.actuation as string;

    const relations = [];
    if (req.query.full) {
      relations.push("device");
      relations.push("group");
      relations.push("event");
      relations.push("actuation");
    }

    const skip = parseInt(String(req.query.skip) || "0");
    const take = parseInt(String(req.query.take) || "0");

    const [data, count] = await Resolve.findAndCount({
      where: where_clause,
      relations,
      skip,
      take,
      order: {
        created_at: "DESC",
      },
    });

    if (req.query.count) return res.json({ count });
    res.json({ data, count });
  } catch (e) {
    res.status(500).send(e);
  }
};

export default data;
