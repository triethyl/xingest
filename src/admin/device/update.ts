import { Request, RequestHandler } from "express";

import Joi from "joi";
import Device from "../../entity/Device";

import DeviceGroup from "../../entity/DeviceGroup";
import customError from "../../utils/customError";

import log from "../../log";

interface RequestBody {
  group_id?: string;
  for_company?: string;
  tags?: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }),
    for_company: Joi.string(),
    tags: Joi.string(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const updateDevice: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const device = await Device.findOne({ id: req.params.id, deleted: false });
  if (!device)
    return customError(
      res,
      '"device" not found',
      "id",
      "not_found",
      req.params.id
    );

  const { group_id, for_company, tags }: RequestBody = req.body;

  if (group_id) {
    const group = await DeviceGroup.findOne({ id: group_id });
    if (!group)
      return customError(res, '"group" not found', "id", "not_found", group_id);

    device.group_id = group_id;
  }

  if (for_company) device.for_company = for_company;
  if (tags) device.tags = tags;

  await device.save();

  res.sendStatus(200);

  log.info("Device updated successfully", device);
};

export default updateDevice;
