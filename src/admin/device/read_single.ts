import { RequestHandler } from "express";

import Device from "../../entity/Device";

const readSingleDevice: RequestHandler = async (req, res) => {
  let data: Device[];

  let deleted = false;
  if (req.query.showDeleted) deleted = true;

  if (req.query.full)
    data = await Device.find({
      where: { id: req.params.id, deleted },
      relations: ["firmware", "group", "group.event"],
    });
  else data = await Device.find({ id: req.params.id, deleted });

  res.json(data);
};

export default readSingleDevice;
