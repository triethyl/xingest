import { Request, RequestHandler } from "express";

import Joi from "joi";

import Device from "../../entity/Device";
import DeviceGroup from "../../entity/DeviceGroup";

import customError from "../../utils/customError";
import deviceCreateMetadata from "../../utils/deviceCreateMetadata";

import log from "../../log";

interface RequestBody {
  group_id: string;
  for_company: string;
  tags: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }).required(),
    for_company: Joi.string().required(),
    tags: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const createDevice: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { group_id, for_company, tags }: RequestBody = req.body;

  const group = await DeviceGroup.findOne({ id: group_id });
  if (!group)
    return customError(res, '"group" not found', "id", "not_found", group_id);

  const dev = deviceCreateMetadata();

  await Device.create({ ...dev, group_id, for_company, tags }).save();

  res.json(dev);

  log.info("Device created successfully", dev);
};

export default createDevice;
