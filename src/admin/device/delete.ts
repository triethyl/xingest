import { RequestHandler } from "express";

import Device from "../../entity/Device";
import customError from "../../utils/customError";

import log from "../../log";

const deleteDevice: RequestHandler = async (req, res) => {
  const device = await Device.findOne({ id: req.params.id, deleted: false });
  if (!device)
    return customError(
      res,
      '"device" not found',
      "id",
      "not_found",
      req.params.id
    );

  device.deleted = true;
  await device.save();

  res.sendStatus(200);

  log.info("Device deleted successfully", device);
};

export default deleteDevice;
