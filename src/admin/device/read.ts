import { RequestHandler } from "express";
import { getConnection } from "typeorm";

import Device from "../../entity/Device";
import Resolve from "../../entity/Resolve";

const readDevice: RequestHandler = async (req, res) => {
  let data: Device[];

  let deleted = false;
  if (req.query.showDeleted) deleted = true;

  if (req.query.full)
    data = await Device.find({
      where: { deleted },
      relations: ["firmware", "group", "group.event"],
    });
  else data = await Device.find({ deleted });

  const device_last_responded = await getConnection()
    .createQueryBuilder(Resolve, "resolve")
    .select("device_id")
    .addSelect("MAX(created_at)", "last")
    .groupBy("device_id")
    .getRawMany();

  const result: any[] = data.map((d) => {
    const i = device_last_responded.findIndex((x) => x.device_id === d.id);
    if (i === -1) return { ...d, last: null };
    else return { ...d, last: device_last_responded[i].last };
  });

  res.json(result);
};

export default readDevice;
