import { RequestHandler } from "express";

import Device from "../../entity/Device";
import customError from "../../utils/customError";

import log from "../../log";

const enableDevice: RequestHandler = async (req, res) => {
  const device = await Device.findOne({ id: req.params.id, deleted: false });
  if (!device)
    return customError(
      res,
      '"device" not found',
      "id",
      "not_found",
      req.params.id
    );

  device.disabled = false;
  await device.save();

  res.sendStatus(200);

  log.info("Device enabled successfully", device);
};

export default enableDevice;
