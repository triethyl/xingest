import { Request, RequestHandler } from "express";

import Joi from "joi";

import customError from "../utils/customError";

import s3 from "../utils/s3";

import log from "../log";

interface RequestBody {
  key: string;
  bucket: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    key: Joi.string().required(),
    bucket: Joi.string().required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const changepw: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { key, bucket }: RequestBody = req.body;

  try {
    const signed_url = await s3.getSignedUrlPromise("getObject", {
      Bucket: bucket,
      Key: key,
      Expires: 20000,
    });

    res.status(200).json({ signed_url });
  } catch {
    customError(res, "S3 pair incorrect", "key", "incorrect", key);
    log.info("Admin change password failed");
  }
};

export default changepw;
