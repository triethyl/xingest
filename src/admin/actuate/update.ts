import { Request, RequestHandler } from "express";

import Joi from "joi";

import Actuate from "../../entity/Actuate";
import DeviceGroup from "../../entity/DeviceGroup";

import customError from "../../utils/customError";

import log from "../../log";

interface RequestBody {
  group_id?: string;
  name?: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }),
    name: Joi.string()
      .regex(/^[a-z0-9-]+$/)
      .invalid("firmware-update"),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const updateActuation: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const actu = await Actuate.findOne({ id: req.params.id, deleted: false });
  if (!actu)
    return customError(
      res,
      '"actuation" not found',
      "id",
      "not_found",
      req.params.id
    );

  const { group_id, name }: RequestBody = req.body;

  if (group_id) {
    const group = await DeviceGroup.findOne({ id: group_id });
    if (!group)
      return customError(res, '"group" not found', "id", "not_found", group_id);

    actu.group_id = group_id;
  }

  if (name) actu.name = name;

  await actu.save();

  res.sendStatus(200);

  log.info("Actuation updated successfully", req.body);
};

export default updateActuation;
