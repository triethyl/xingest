import { RequestHandler } from "express";

import Actuate from "../../entity/Actuate";
import customError from "../../utils/customError";

import log from "../../log";

const deleteActuation: RequestHandler = async (req, res) => {
  const actu = await Actuate.findOne({ id: req.params.id, deleted: false });
  if (!actu)
    return customError(
      res,
      '"actuation" not found',
      "id",
      "not_found",
      req.params.id
    );

  actu.deleted = true;
  await actu.save();

  res.sendStatus(200);
  log.info("Actuation deleted successfully", actu);
};

export default deleteActuation;
