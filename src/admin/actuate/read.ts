import { RequestHandler } from "express";

import Actuate from "../../entity/Actuate";

const readActuation: RequestHandler = async (req, res) => {
  let data: Actuate[];

  let deleted = false;
  if (req.query.showDeleted) deleted = true;

  if (req.query.full)
    data = await Actuate.find({
      where: { deleted },
      relations: ["group", "group.device"],
    });
  else data = await Actuate.find({ deleted });

  res.json(data);
};

export default readActuation;
