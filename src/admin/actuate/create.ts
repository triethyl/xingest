import { Request, RequestHandler } from "express";
import str from "@supercharge/strings";

import Joi from "joi";

import Actuate from "../../entity/Actuate";
import DeviceGroup from "../../entity/DeviceGroup";

import customError from "../../utils/customError";

import log from "../../log";

interface RequestBody {
  group_id: string;
  name: string;
}

const validate = (req: Request) => {
  const schema = Joi.object({
    group_id: Joi.string().uuid({ version: "uuidv4" }).required(),
    name: Joi.string()
      .regex(/^[a-z0-9-]+$/)
      .invalid("firmware-update")
      .required(),
  });

  return schema.validate(req.body, { abortEarly: false });
};

const createActuation: RequestHandler = async (req, res) => {
  const { error } = validate(req);
  if (error) return res.status(400).json(error.details);

  const { group_id, name }: RequestBody = req.body;

  const group = await DeviceGroup.findOne({ id: group_id });
  if (!group)
    return customError(res, '"group" not found', "id", "not_found", group_id);

  const auth = str.random(50);

  await Actuate.create({ group_id, name, auth }).save();

  res.json({ auth });

  log.info("Actuation created successfully", req.body);
};

export default createActuation;
