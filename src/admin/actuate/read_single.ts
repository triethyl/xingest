import { RequestHandler } from "express";

import Actuate from "../../entity/Actuate";

const readSingleActuation: RequestHandler = async (req, res) => {
  let data: Actuate[];

  let deleted = false;
  if (req.query.showDeleted) deleted = true;

  if (req.query.full)
    data = await Actuate.find({
      where: { id: req.params.id, deleted },
      relations: ["group", "group.device"],
    });
  else data = await Actuate.find({ id: req.params.id, deleted });

  res.json(data);
};

export default readSingleActuation;
