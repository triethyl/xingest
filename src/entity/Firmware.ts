import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";

import Device from "./Device";
import DeviceGroup from "./DeviceGroup";

@Entity()
@Unique(["version", "group_id"])
export default class Firmware extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  version: string;

  @Column()
  s3_bucket: string;

  @Column()
  s3_key: string;

  @Column({ default: false })
  latest: boolean;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @OneToMany(() => Device, (device) => device.firmware)
  device: Device[];

  @Column()
  group_id: string;
  @ManyToOne(() => DeviceGroup, (group) => group.device)
  @JoinColumn({ name: "group_id" })
  group: DeviceGroup;
}
