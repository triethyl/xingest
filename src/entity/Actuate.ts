import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

import DeviceGroup from "./DeviceGroup";
import Resolve from "./Resolve";

@Entity()
export default class Actuate extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column()
  auth: string;

  @Column({ default: false })
  deleted: boolean;

  @OneToMany(() => Resolve, (resolve) => resolve.actuation)
  resolve: Resolve[];

  @Column()
  group_id: string;
  @ManyToOne(() => DeviceGroup, (group) => group.event)
  @JoinColumn({ name: "group_id" })
  group: DeviceGroup;
}
