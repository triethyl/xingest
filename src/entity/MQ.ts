import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

import Event from "./Event";

@Entity()
export default class MQ extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column()
  event_id: string;
  @ManyToOne(() => Event, (event) => event.mq)
  @JoinColumn({ name: "event_id" })
  event: Event;
}
