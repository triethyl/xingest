import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

import DeviceGroup from "./DeviceGroup";
import MQ from "./MQ";
import Resolve from "./Resolve";

export enum EventBody {
  text = "text",
  json = "json",
  file = "file",
}

@Entity()
export default class Event extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column({ type: "enum", enum: EventBody })
  body: EventBody;

  @Column({ nullable: true, type: "varchar" })
  file_mimetype?: string | null;

  @Column({ nullable: true, type: "boolean" })
  file_multiple?: boolean | null;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @Column({ default: false })
  deleted: boolean;

  @OneToMany(() => MQ, (mq) => mq.event)
  mq: MQ[];

  @OneToMany(() => Resolve, (resolve) => resolve.event)
  resolve: Resolve[];

  @Column()
  group_id: string;
  @ManyToOne(() => DeviceGroup, (group) => group.event)
  @JoinColumn({ name: "group_id" })
  group: DeviceGroup;
}
