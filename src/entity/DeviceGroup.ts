import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";

import Device from "./Device";
import Event from "./Event";
import Firmware from "./Firmware";
import Resolve from "./Resolve";

@Entity()
@Unique(["platform", "name"])
export default class DeviceGroup extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column()
  purpose: string;

  @Column()
  platform: string;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @OneToMany(() => Event, (event) => event.group)
  event: Event[];

  @OneToMany(() => Device, (device) => device.group)
  device: Device[];

  @OneToMany(() => Firmware, (firmware) => firmware.group)
  firmware: Firmware[];

  @OneToMany(() => Resolve, (resolve) => resolve.group)
  resolve: Resolve[];
}
