import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

import Event from "./Event";
import Actuate from "./Actuate";
import DeviceGroup from "./DeviceGroup";
import Device from "./Device";

export enum TransportType {
  http = "http",
  mqtt = "mqtt",
}

@Entity()
export default class Resolve extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  type: string;

  @Column()
  body: string;

  @Column({ default: false })
  is_actuation: boolean;

  @Column({ type: "enum", enum: TransportType, default: TransportType.http })
  transport: TransportType;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @Column({ nullable: true })
  event_id: string;
  @ManyToOne(() => Event, (event) => event.resolve)
  @JoinColumn({ name: "event_id" })
  event: Event;

  @Column({ nullable: true })
  actuation_id: string;
  @ManyToOne(() => Actuate, (actu) => actu.resolve)
  @JoinColumn({ name: "actuation_id" })
  actuation: Actuate;

  @Column()
  group_id: string;
  @ManyToOne(() => DeviceGroup, (group) => group.resolve)
  @JoinColumn({ name: "group_id" })
  group: DeviceGroup;

  @Column()
  device_id: string;
  @ManyToOne(() => Device, (device) => device.resolve)
  @JoinColumn({ name: "device_id" })
  device: Device;
}
