import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

import DeviceGroup from "./DeviceGroup";
import Firmware from "./Firmware";
import Resolve from "./Resolve";

@Entity()
export default class Device extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  device_id: string;

  @Column()
  device_password: string;

  @Column()
  device_hash: string; // SHA256(device_id::device_password)

  @Column()
  for_company: string;

  @Column()
  tags: string;

  @Column({ default: false })
  disabled: boolean;

  @Column({ default: false })
  deleted: boolean;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @OneToMany(() => Resolve, (resolve) => resolve.device)
  resolve: Resolve[];

  @Column({ nullable: true })
  firmware_id: string;
  @ManyToOne(() => Firmware, (firmware) => firmware.device)
  @JoinColumn({ name: "firmware_id" })
  firmware: Firmware;

  @Column()
  group_id: string;
  @ManyToOne(() => DeviceGroup, (group) => group.device)
  @JoinColumn({ name: "group_id" })
  group: DeviceGroup;
}
