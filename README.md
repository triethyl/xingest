# xIngest Docs

## Transport

xIngest allows events to use both [HTTP API](https://api.xingest.durbinservices.com) as well as [MQTT](https://mqtt.xingest.durbinservices.com).

### MQTT Protocol

#### Event

After securely connecting with the servers, one can publish to events in the following fashion:

**TOPIC:** `<platform>/<group>/<event>/<deviceId>`

**PAYLOAD:**

```
<devicePassword>
data
```

#### Actuation

For actuation signals from xIngest, the device needs to subcribe to triggers in the following fashion:

**TOPIC:** `<platform>/<group>/<actuation>/<deviceId>`

It will receive only the data sent in the trigger as the payload.

#### Firmware

On a Firmware update (marked latest), xIngest will create the signed URL and publish to all the devices using MQTT.

To listen to this firmware updates, one needs to subscribe to the following topic:

**TOPIC:** `<platform>/<group>/firmware-update/<deviceId>`

The payload will contain a single signed URL like "https://something.s3.com/...". The device can OTA Update using this URL.

Also, if a device wants to explicitly know if any updates are there, it can publish in this way:

**TOPIC:** `<platform>/<group>/firmware-fetch/<deviceId>`

**PAYLOAD:**

```
<devicePassword>
<currentVersion>
```

If there in an update, it will publish to `<platform>/<group>/firmware-update/<deviceId>` in the similar manner as described above.
